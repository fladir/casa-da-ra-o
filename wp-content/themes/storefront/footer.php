<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */
$logo_alt = get_theme_mod('alter_logo');
$grupoFooter = get_field('grupo_footer', 'options');
$grupoContatos = get_field('grupo_informacoes_para_contato', 'options');
$emails = $grupoContatos['emails'];
$telefones = $grupoContatos['telefones'];
$enderecos = $grupoContatos['enderecos'];
$redesSociais = $grupoContatos['redes_sociais'];

?>

</div><!-- .col-full -->
</div><!-- #content -->

<?php do_action('storefront_before_footer'); ?>
<footer id="colophon" class="site-footer" role="contentinfo"
        style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/background-footer.png); background-position: 0 -100px;">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <img src="<?php echo wp_get_attachment_image_url($logo_alt, 'logo_footer'); ?>"
                     class="black-bg-logo mb-4" alt="<?php echo get_bloginfo('name'); ?>">
                <p class="pr-3">
                    <?php echo $grupoFooter['codigo_footer'] ?>
                </p>
            </div>
            <div class="col-md-3">
                <h6 class="text-uppercase">Navegue</h6>

                <?php
                wp_nav_menu(array(
                    'theme_location' => 'footer_1',
                    'depth' => 3,
                    'container' => 'div',
                    'container_class' => '',
                    'container_id' => 'menu-footer',
                    'menu_class' => 'navbar-nav ml-auto',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker(),
                ));
                ?>
            </div>
            <div class="col-md-3">
                <h6 class="text-uppercase">Institucional</h6>

                <?php
                wp_nav_menu(array(
                    'theme_location' => 'footer_2',
                    'depth' => 3,
                    'container' => 'div',
                    'container_class' => '',
                    'container_id' => 'menu-footer',
                    'menu_class' => 'navbar-nav ml-auto',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker(),
                ));
                ?>
            </div>
            <div class="col-md-3">
                <h6 class="text-uppercase">Contato</h6>
                <p>
                    <?php foreach ($emails

                    as $email) : ?>
                <p>
                    <a href="mailto:<?php echo $email['endereco_email'] ?>" target="_blank">
                        <?php echo $email['endereco_email'] ?>
                    </a>
                </p>
                <?php endforeach; ?>
                <?php foreach ($telefones as $telefone) : ?>
                    <p>
                        <a href="tel:<?php echo $telefone['numero_telefone'] ?>" target="_blank">
                            <?php echo $telefone['numero_telefone'] ?>
                        </a>
                    </p>
                <?php endforeach; ?>
                <?php foreach ($enderecos as $endereco) : ?>
                    <p>
                        <?php echo $endereco['endereco'] ?>
                    </p>
                <?php endforeach; ?>
                <div class="redes-sociais d-flex">
                    <?php foreach ($redesSociais as $redesSocial) : ?>


                        <a href="<?php echo $redesSocial['link_social'] ?>" target="_blank"
                           title="<?php echo $redesSocial['nome_rede_social'] ?>"
                           alt="<?php echo $redesSocial['nome_rede_social'] ?>">
                            <?php echo $redesSocial['icone_social'] ?>
                        </a>


                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container bottom-footer mt-5">
        <div class="row">
            <div class="col-md-6">
                <h5>FORMAS DE PAGAMENTO</h5>
                <img src="<?php echo wp_get_attachment_image_url(get_field('grupo_footer', 'options')['formas_de_pagamento'], 'full'); ?>"
                     alt="">
            </div>
            <div class="col-md-6 d-flex justify-content-end align-items-center">
                <a href="https://ciawebsites.com.br" target="_blank">

                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo-cia-cinza.png"
                         alt="Cia Websites - Otimização de sites em BH"
                         title="Cia Websites - Otimização de sites em BH">
                </a>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->

<?php do_action('storefront_after_footer'); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
