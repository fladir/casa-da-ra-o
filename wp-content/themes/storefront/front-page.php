<?php get_header() ?>
</div>
</div>

<!-- Banner/Slide -->
<?php get_template_part('components/homepage/slide-bootstrap/slide'); ?>

<!-- Destaques  -->
<?php get_template_part('components/homepage/destaques'); ?>

<!-- Top Destaques  -->
<?php get_template_part('components/homepage/top-destaques'); ?>

<!-- Marcas Favoritas  -->
<?php get_template_part('components/homepage/marcas-favoritas'); ?>

<!-- Promocoes Imperdíveis  -->
<?php get_template_part('components/homepage/promocoes-imperdiveis'); ?>

<!-- Mais Promoções  -->
<?php get_template_part('components/homepage/mais-promocoes'); ?>

<!-- Depoimentos  -->
<?php get_template_part('components/homepage/depoimentos'); ?>

<!-- Porque Escolher a Casa da Ração  -->
<?php get_template_part('components/homepage/porque-escolher'); ?>

<!-- Blog  -->
<?php get_template_part('components/homepage/blog'); ?>

<!-- Newsletter  -->
<?php get_template_part('components/homepage/newsletter'); ?>

<?php get_footer() ?>
