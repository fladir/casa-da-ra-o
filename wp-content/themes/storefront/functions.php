<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme = wp_get_theme('storefront');
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */

if (!isset($content_width)) {
    $content_width = 980; /* pixels */
}

$storefront = (object)array(
    'version' => $storefront_version,

    /**
     * Initialize all the things.
     */
    'main' => require 'inc/class-storefront.php',
    'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';
require 'inc/wordpress-shims.php';

if (class_exists('Jetpack')) {
    $storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if (storefront_is_woocommerce_activated()) {
    $storefront->woocommerce = require 'inc/woocommerce/class-storefront-woocommerce.php';
    $storefront->woocommerce_customizer = require 'inc/woocommerce/class-storefront-woocommerce-customizer.php';

    require 'inc/woocommerce/class-storefront-woocommerce-adjacent-products.php';

    require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
    require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
    require 'inc/woocommerce/storefront-woocommerce-functions.php';
}

if (is_admin()) {
    $storefront->admin = require 'inc/admin/class-storefront-admin.php';

    require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if (version_compare(get_bloginfo('version'), '4.7.3', '>=') && (is_admin() || is_customize_preview())) {
    require 'inc/nux/class-storefront-nux-admin.php';
    require 'inc/nux/class-storefront-nux-guided-tour.php';

    if (defined('WC_VERSION') && version_compare(WC_VERSION, '3.0.0', '>=')) {
        require 'inc/nux/class-storefront-nux-starter-content.php';
    }
}

/**
 * Custom Functions CIA Themes
 */

# FRONT END
function pbo_load_css()
{
    wp_enqueue_style('owlCarousel', get_template_directory_uri() . '/assets/js/owl.carousel/dist/assets/owl.carousel.min.css', array());
    wp_enqueue_style('customCSS', get_template_directory_uri() . '/assets/css/style.css', array());
    wp_enqueue_style('fontAwesome', get_template_directory_uri() . '/assets/css/@fortawesome/fontawesome-free/css/all.css', array());
}
add_action('wp_enqueue_scripts', 'pbo_load_css');


add_filter( 'get_product_search_form' , 'woo_custom_product_searchform' );
function woo_custom_product_searchform( $form )
{
    $form = '<form role="search" method="get" id="searchform" action="' . esc_url(home_url('/')) . '">
    <div>
      <label class="screen-reader-text" for="s">' . __('Search for:', 'woocommerce') . '</label>
      <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Encontre o que seu pet precisa" />
            <input type="hidden" name="post_type" value="product" />
            <button type="submit">Busca</button>
    </div>
  </form>';
    return $form;
}

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title'   => 'Configurações',
        'menu_title'  => 'Configurações do Tema',
        'menu_slug'   => 'theme-general-settings',
        'capability'  => 'edit_posts',
        'icon_url'      => get_template_directory_uri() . '/assets/images/favicon_cia.png',
        'position'      => '1000',
        'redirect'    => false
    ));
}


function pbo_load_js()
{
    wp_enqueue_script('owlCarousel', get_template_directory_uri() . '/assets/js/owl.carousel/dist/owl.carousel.min.js', array('jquery'), 1, true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/scss/bootstrap/dist/js/bootstrap.min.js', array('jquery'), 1, true);
    wp_enqueue_script('cutomJS', get_template_directory_uri() . '/assets/js/custom.js', array('jquery'), 1, true);
}

add_action('wp_enqueue_scripts', 'pbo_load_js');

# thumbnails
require 'core/functions_thumbnails.php';

// Custom product taxonomy Woocommerce
add_action( 'init', 'create_product_taxonomies', 0 );

function create_product_taxonomies() {
    $labels = array(
        'name'              => _x( 'Marca', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'Marcas', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search Marcas', 'textdomain' ),
        'all_items'         => __( 'All Marca', 'textdomain' ),
        'parent_item'       => __( 'Parent Marcas', 'textdomain' ),
        'parent_item_colon' => __( 'Parent Marcas:', 'textdomain' ),
        'edit_item'         => __( 'Edit Marcas', 'textdomain' ),
        'update_item'       => __( 'Update Marcas', 'textdomain' ),
        'add_new_item'      => __( 'Add New Marcas', 'textdomain' ),
        'new_item_name'     => __( 'New Marca Name', 'textdomain' ),
        'menu_name'         => __( 'Marcas', 'textdomain' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'marcas' ),
    );

    register_taxonomy( 'marca', array( 'product' ), $args );


}





function my_first_custom_post_type() {

// Custom post type Depoimentos
    $labels = array(
        'name'                => _x( 'Depoimentos', 'Post Type General Name', 'storeftont' ),
        'singular_name'       => _x( 'Depoimento', 'Post Type Singular Name', 'storeftont' ),
        'menu_name'           => __( 'Depoimentos', 'storeftont' ),
        'all_items'           => __( 'Todod os Depoimentos', 'storeftont' ),
        'view_item'           => __( 'Ver Depoimento', 'storeftont' ),
        'add_new_item'        => __( 'Adicionar novo Depoimento', 'storeftont' ),
        'add_new'             => __( 'Adicionar Novo', 'storeftont' ),
        'edit_item'           => __( 'Editar Depoimento', 'storeftont' ),
        'update_item'         => __( 'Atualizar Depoimento', 'storeftont' ),
        'search_items'        => __( 'Buscar Depoimento', 'storeftont' ),
        'not_found'           => __( 'Não Encontrado', 'storeftont' ),
        'not_found_in_trash'  => __( 'Não encontrado na lixeira', 'storeftont' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'depoimentos'),
        'description'         => __( 'Depoimentos de Clientes', 'storefront' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor' ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'depoimentos' ),
        'menu_icon' => 'dashicons-format-chat',
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 50,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    // Registering your Custom Post Type
    register_post_type( 'depoimentos', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/


add_action( 'init', 'my_first_custom_post_type', 10   );

if ( ! function_exists( 'is_short_content' ) )
{
    /**
     * Is the passed string not longer than an excerpt?
     *
     * @param  string $content
     * @return boolean
     */
    function is_short_content( $content = NULL )
    {
        NULL === $content && $content = get_the_content();
        // Get maximal excerpt length for this theme
        $max_word_length = apply_filters( 'excerpt_length', 55 );

        // see 'Counting words in a post'
        // https://wordpress.stackexchange.com/a/52460/73
        $content_text    = trim( strip_tags( $content ) );
        $content_words   = preg_match_all( '~\s+~', "$content_text ", $m );

        return $content_words <= $max_word_length;
    }
}

if ( ! function_exists( 't5_show_short_content' ) )
{
    add_filter( 'the_excerpt', 't5_show_short_content' );

    /**
     * Print the whole content if the visible words are less than the excerpt length.
     *
     * @param  string $excerpt
     * @return string
     */
    function t5_show_short_content( $excerpt )
    {
        $content = apply_filters( 'the_content', get_the_content() );
        $content = str_replace( ']]>', ']]&gt;', $content );
        return is_short_content( $content ) ? $content : $excerpt;
    }
}

function custom_excerpt_length( $length ) {
    return 37;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// Mudando a aparência das reticências ([…])
function newExcerptMore($more){
    return '…';
}
add_filter('excerpt_more', 'newExcerptMore');

function customize_ciaprime($wp_customize){
    // Logo alternativa
    $wp_customize->add_setting('alter_logo', array(
        'default'   => '',
        'type'  => 'theme_mod'
    ));
    $wp_customize->add_control(new WP_Customize_Media_Control($wp_customize, 'alter_logo', array(
            'label' => sprintf('Logo do Rodapé', 'Cia Prime'),
            'section'   => 'title_tagline',
            'mime_type' => 'image',
            'settings' => 'alter_logo',
            'priority'  => 9,
        )
    ));


}
add_action('customize_register', 'customize_ciaprime');

# Funções principais do tema
require 'core/functions/wp-bootstrap-nav-walker.php';

function pbo_settings_theme()
{

    register_nav_menus(array(
        'footer_1' => __('Footer (Navegue)', 'CiaWebsites'),
        'footer_2' => __('Footer (Institucional)', 'CiaWebsites'),
    ));
}
add_action('init', 'pbo_settings_theme');