<?php
$destaques = get_field('destaques');
?>
<?php if ($destaques) : ?>
    <section class="destaques-home">
        <div class="container">
            <div class="row">
                <?php foreach ($destaques as $destaque) : ?>
                    <div class="col-md content-destaques">
                        <div class="row">
                            <div class="col-3">
                                <img src="<?php echo $destaque['icone']['sizes']['icon_destaque'] ?>" alt="">
                            </div>
                            <div class="col-9">
                                <?php if ($destaque['titulo']) : ?>
                                    <p class="titulo-destaques"><?php echo $destaque['titulo'] ?></p>
                                <?php endif; ?>
                                <?php if ($destaque['subtitulo']) : ?>
                                    <p class="subtitulo-destaques"><?php echo $destaque['subtitulo'] ?></p>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>