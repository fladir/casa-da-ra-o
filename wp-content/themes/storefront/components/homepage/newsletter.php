<section class="newsletter-home">

    <div class="container">

        <div class="row position-relative px-5">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/fundo-newsletter.jpg"
                 class="fundo-newsletter"/>

            <div class="col-12 frase-newsletter mb-4 mt-4">
                <h3 class="text-uppercase titulo-newsletter"><?php echo get_field('titulo_newsletter') ?></h3>
                <h4 class="text-uppercase subtitulo-newsletter"><?php echo get_field('subtitulo_newsletter') ?></h4>
            </div>
            <div class="col-md-9">
                <?php echo do_shortcode('[get_news_letter_mail mail="Email" buttom="Enviar"]') ?>
            </div>
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/dog-newsletter.png"
                 class="dog-newsletter"/>


        </div>


    </div>

</section>

<?php /* Restore original Post Data */
wp_reset_postdata(); ?>