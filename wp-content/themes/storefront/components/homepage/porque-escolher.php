<?php
$caixas = get_field('porque_escolher_caixas', 188)
?>
<?php if ($caixas) : ?>
    <section class="porque-escolher">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex align-items-center pl-0 mb-5">
                    <div class="top-destaques-bg d-flex align-items-center">
                        <img style="height: 24px; width: 26px"
                             src="<?php bloginfo('template_directory'); ?>/assets/images/pata-titulo.png"
                             class="logo-cia mr-2"/>
                        <h3 class="titulo-secao text-uppercase"><?php echo get_field('titulo_da_secao_porque_escolher', 188) ?></h3>
                    </div>
                    <div class="divisor"></div>
                </div>
                <?php foreach ($caixas as $caixa) : ?>
                    <div class="col-md">
                        <div class="box-content p-4" style="background-color: <?php echo $caixa['cor_da_caixa'] ?>">
                            <div class="topo-box" style="background-color: <?php echo $caixa['cor_do_topo'] ?> "></div>
                            <img src="<?php echo wp_get_attachment_image_url($caixa['icone'], 'icon_porque_escolher'); ?>"
                                 alt="">
                            <h4><?php echo $caixa['titulo']; ?></h4>
                            <p><?php echo $caixa['texto']; ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>