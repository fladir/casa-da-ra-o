<?php
$args = array(
    'post_type' => 'depoimentos',
    'order' => 'ASC'
);
$WPQuery = new WP_Query($args);
?>
    <section class="depoimentos mt-5">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex align-items-center pl-0">
                    <div class="top-destaques-bg d-flex align-items-center">
                        <img style="height: 24px; width: 26px"
                             src="<?php bloginfo('template_directory'); ?>/assets/images/pata-titulo.png"
                             class="logo-cia mr-2"/>
                        <h3 class="titulo-secao text-uppercase">O que os nossos clientes Dizem</h3>
                    </div>
                    <div class="divisor"></div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 px-0">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/img-depoimentos.png"/>

                </div>
                <div class="col-md-6 content-testmonial">
                    <div class="owl-carousel owl-theme carousel-depoimentos">
                        <?php $i = 0;
                    if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                            <?php $pet = get_field('nome_do_pet'); ?>
                            <div class="item">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/aspas-depoimentos.png"
                                     class="aspas-depoimentos"/>
                                <?php the_excerpt(); ?>


                                <?php if (!is_short_content()) : ?>
                                    <a class="testmonialmodal" href="#" rel="nofollow noreferrer noopener external"
                                       data-toggle="modal" data-target="#testimonialmodal<?php echo $i; ?>">Leia
                                        Mais</a>
                                <?php endif; ?>



                                <p class="titulo-destaques"><?php the_title() ?></p>
                                <p class="especie-nome-pet"><?php echo $pet ?></p>
                            </div>
                        <?php $i++; endwhile; endif; ?>
                    </div>
                </div>
            </div>
            <?php $i = 0;
            if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                <?php $pet = get_field('nome_do_pet'); ?>
                <div class="modal modalcomentarios fade" id="testimonialmodal<?php echo $i; ?>" tabindex="-1"
                     role="dialog" aria-labelledby="#testimonialmodal<?php echo $i; ?>"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="wrapper-form-modal p-3 pt-5 text-center">
                                <?php the_post_thumbnail(); ?>
                                <?php the_content(); ?>
                                <h5 class="fw-semi-bold mb-0 title-testmonials-modal"><?php the_title(); ?></h5>
                                <p class="especie-nome-pet"><?php echo $pet ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++; endwhile; endif; ?>
    </section>
