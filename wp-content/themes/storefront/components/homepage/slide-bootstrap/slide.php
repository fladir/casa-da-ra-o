<?php
$slides = get_field('slide_repetidor');

?>

    <section class="slides">
        <div class="owl-carousel owl-theme slider-home-carousel">


            <?php foreach ($slides as $slide) : ?>
                <div class="item">
                    <a target="_blank" class="link-portfolio" data-fancybox="galeria<?php echo $i; ?>" <?php if($slide['link_do_slide']) : ?>href="<?php echo $slide['link_do_slide']; ?>" <?php endif; ?>>
                        <img src="<?php echo $slide['imagem']['sizes']['banner']; ?>" alt="">
                    </a>

                </div>
            <?php endforeach; ?>


        </div>

    </section>
