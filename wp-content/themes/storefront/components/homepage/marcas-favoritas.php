<?php
$args = array(
    'taxonomy' => 'marca',
    'hide_empty' => 0
);
$cats = get_terms($args);
?>
<section class="marcas-favoritas mt-4 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="top-destaques-bg d-flex align-items-center mb-3">
                    <img style="height: 24px; width: 26px"
                         src="<?php bloginfo('template_directory'); ?>/assets/images/pata-titulo.png"
                         class="logo-cia mr-2"/>
                    <h3 class="titulo-secao text-uppercase">MARCAS FAVORITAS</h3>
                </div>
                <div class="owl-carousel owl-theme carousel-marcas-favoritas">
                    <?php foreach ($cats as $cat) : ?>
                        <?php $img = get_field('logomarca', $cat); ?>

                        <div class="item">

                            <a href="<?php echo get_term_link($cat->slug, 'marca') ?>">
                            <img src="<?php echo wp_get_attachment_image_url($img, 'marcas_favoritas'); ?>"
                                 alt="<?php echo $cat->name; ?>">
                            </a>

                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
