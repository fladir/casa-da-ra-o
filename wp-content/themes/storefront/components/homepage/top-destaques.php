<?php
$banners = get_field('banners_top_destaques')
?>
<?php if ($banners) : ?>
    <section class="top-destaques-home mt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex align-items-center pl-0">
                    <div class="top-destaques-bg d-flex align-items-center">
                        <img style="height: 24px; width: 26px"
                             src="<?php bloginfo('template_directory'); ?>/assets/images/pata-titulo.png"
                             class="logo-cia mr-2"/>
                        <h3 class="titulo-secao text-uppercase"><?php echo get_field('titulo_da_secao_top_destaques') ?></h3>
                    </div>
                    <div class="divisor"></div>
                </div>

                <?php foreach ($banners as $banner) : ?>
                    <div class="col-md mt-4">
                        <a <?php if ($banner['link_do_banner']) : ?>href="<?php echo $banner['link_do_banner'] ?>"<?php endif; ?> >
                            <img src="<?php echo $banner['imagem_do_banner_top']['sizes']['banner_top_destaque'] ?>"
                                 alt="">
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>