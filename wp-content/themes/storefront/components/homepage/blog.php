<?php
$args = array(
    'nopaging'       => false,
    'paged'          => $paged,
    'post_type'      => 'post',
    'posts_per_page' => 3,
    'orderby'        => 'post_date',
    'order'          => 'DESC'
);

$WPQuery = new WP_Query( $args );
?>
<section class="blog-home mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex align-items-center pl-0 mb-5">
                <div class="top-destaques-bg d-flex align-items-center">
                    <img style="height: 24px; width: 26px"
                         src="<?php bloginfo('template_directory'); ?>/assets/images/pata-titulo.png"
                         class="logo-cia mr-2"/>
                    <h3 class="titulo-secao text-uppercase">Blog</h3>
                </div>
                <div class="divisor"></div>
            </div>

            <?php if ( $WPQuery->have_posts() ) : while ( $WPQuery->have_posts() ) : $WPQuery->the_post(); ?>

                <div class="col-md-4 mb-4">
                    <div class="card">
                        <div class="wrapper-blog-data">
                            <img style="height: 24px; width: 26px"
                                 src="<?php bloginfo('template_directory'); ?>/assets/images/icon-pata.png"
                                 class="pata-data"/>

                            <p><span class="mes"><?php echo get_the_date('M');
                                echo '</span><br><span class="dia">';
                                echo get_the_date('d');?></span></p>
                        </div>
                        <a href="<?php the_permalink(); ?>" class="link-img-home">
                            <?php the_post_thumbnail( 'blog_small', array( 'alt'   => '' . get_the_title() . '', 'title' => '' . get_the_title() . '' ) ); ?>
                        </a>
                        <div class="card-body">
                            <a href="<?php the_permalink(); ?>" class="link-blog-home">
                                <h5 class="card-title"><?php the_title() ?></h5>
                            </a>

                            <?php the_excerpt(); ?>
                        </div>
                    </div>
                </div>

            <?php
            endwhile;
            endif;
            wp_reset_postdata();
            ?>
        </div>
    </div>
</section>