<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<?php do_action('storefront_before_site'); ?>

<?php
$contatos = get_field('grupo_informacoes_para_contato', 'options');
$emails = $contatos['emails'];
$whatsapps = $contatos['whatsapp'];
?>

<div id="page" class="hfeed site">
    <?php do_action('storefront_before_header'); ?>

    <header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-11 offset-md-2 pt-4 topo-contatos">
                    <div class="row">
                        <div class="col-md-6 contatos">
                            <?php foreach ($emails as $email) : ?>
                                <i class="far fa-envelope"></i>
                                <a href="mailto:<?php echo $email['endereco_email'] ?>">
                                <?php echo $email['endereco_email'] ?>
                                </a>
                            <?php endforeach; ?>
                            <?php foreach ($whatsapps as $whatsapp) : ?>
                                <i class="fab fa-whatsapp ml-5"></i>
                                <a href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp['link_whatsapp'] ?>&text=l%C3%A1%2C%20tudo%20bem%3F">
                                    <?php echo $whatsapp['numero_whatsapp'] ?>
                                </a>
                            <?php endforeach; ?>
                        </div>
                        <div class="col-md-6">
                            <ul class="myaccount-menu">
                                <li>
                                    <?php if ( is_user_logged_in() ) { ?>
                                        <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="Minha Conta">Minha Conta</a>
                                    <?php }
                                    else { ?>
                                        <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>"><?php _e('Login / Register','woothemes'); ?></a>
                                    <?php } ?>
                                </li>
                                |
                                <li>
                                    <a href="#">Meus Pedidos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        /**
         * Functions hooked into storefront_header action
         *
         * @hooked storefront_header_container                 - 0
         * @hooked storefront_skip_links                       - 5
         * @hooked storefront_social_icons                     - 10
         * @hooked storefront_site_branding                    - 20
         * @hooked storefront_secondary_navigation             - 30
         * @hooked storefront_product_search                   - 40
         * @hooked storefront_header_container_close           - 41
         * @hooked storefront_primary_navigation_wrapper       - 42
         * @hooked storefront_primary_navigation               - 50
         * @hooked storefront_header_cart                      - 60
         * @hooked storefront_primary_navigation_wrapper_close - 68
         */
        do_action('storefront_header');
        ?>

    </header><!-- #masthead -->

    <?php
    /**
     * Functions hooked in to storefront_before_content
     *
     * @hooked storefront_header_widget_region - 10
     * @hooked woocommerce_breadcrumb - 10
     */
    do_action('storefront_before_content');
    ?>

    <div id="content" class="site-content" tabindex="-1">
        <div class="col-full">


<?php
do_action('storefront_content_top');
