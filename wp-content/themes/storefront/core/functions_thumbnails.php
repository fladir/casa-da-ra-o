<?php
// Enable post thumbnails
add_theme_support('post-thumbnails');

// Custom image sizes
if (function_exists('add_image_size')) {
    add_image_size('logo_header', 360, 64);
    add_image_size('banner', 1920, 470);
    add_image_size('imagens_col6', 960, 590, true);
    add_image_size('diferenciais', 162, 145);
    add_image_size('ic_chamada', 790, 790);
    add_image_size('secoes_alternadas', 485, 350);
    add_image_size('footer_logo', 374, 68);
    add_image_size('portfolio', 360, 360, true);
    add_image_size('portfolio_big', 800, 600, true);
    add_image_size('como_funciona', 410, 297);
    add_image_size('depoimentos', 150, 150, true);
    add_image_size('calculadora', 914, 626);
    add_image_size('icon_destaque', 45, 45);
    add_image_size('banner_top_destaque', 655, 655);
    add_image_size('promocoes_imperdiveis', 442, 354);
    add_image_size('mais_promocoes', 652, 354);
    add_image_size('marcas_favoritas', 185, 120);
    add_image_size('icon_porque_escolher', 90, 65);
    add_image_size('blog_small', 350, 220);
    add_image_size('logo_footer', 355, 95);


}