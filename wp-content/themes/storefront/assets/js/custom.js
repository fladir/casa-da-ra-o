(function ($) {
    $(document).ready(function () {
        $(".slider-home-carousel").owlCarousel({
            loop: true,
            items: 1,
            lazyLoad: true,
            nav: true,
            dots: false,
            autoplay: false,
            autoplaySpeed: 500,
            slideBy: 1,
            autoplayHoverPause: true,
            navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            mouseDrag: false
        });

        $(".carousel-marcas-favoritas").owlCarousel({
            loop: true,
            items: 6,
            lazyLoad: true,
            nav: true,
            dots: false,
            autoplay: true,
            autoplaySpeed: 500,
            slideBy: 1,
            autoplayHoverPause: true,
            navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
        });

        $(".carousel-depoimentos").owlCarousel({
            loop: true,
            items: 1,
            lazyLoad: true,
            nav: false,
            dots: false,
            autoplay: true,
            autoplaySpeed: 500,
            slideBy: 1,
            autoplayHoverPause: true,
            navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
        });
    });

})(jQuery);
