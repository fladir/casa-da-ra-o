<section class="newsletter">
    <div class="get-news-letter-mail">
        <?php if (isset($status) && $status) : ?>
            <div class="alert alert-primary" role="alert">
                <?php echo nl2br($status); ?>
            </div>
        <?php endif; ?>
        <form id="form-news-letter" class="signup form-inline justify-content-center"
              action="<?php echo get_the_permalink(); ?>#form-news-letter" method="post" enctype="multipart/form-data">

            <div class="row w-100">
                <div class="col-12 col-md-4 text-center text-white pl-0">
                    <div class="form-group">
                        <input name="news_letter_name" type="text" class="input-form mb-3 w-100"
                               style="padding: 14px !important;"
                               value="<?php echo isset($_POST['news_letter_name']) ? $_POST['news_letter_name'] : ''; ?>"
                               placeholder="<?php echo isset($atts['name']) ? $atts['name'] : 'NOME'; ?>">
                    </div>
                </div>

                <div class="col-12 col-md-4 text-center text-white pl-0">
                    <div class="form-group">
                        <input name="news_letter_mail" type="email" class="input-form mb-3 w-100"
                               style="padding: 14px !important;"
                               value="<?php echo isset($_POST['news_letter_mail']) ? $_POST['news_letter_mail'] : ''; ?>"
                                   placeholder="E-MAIL">
                    </div>
                </div>

                <div class="col-12 col-md-4 text-center text-white pl-0">
                    <div class="form-group">
                        <button style="width: 100%" type="submit" class="btn btn-newsletter">
                            <span class="button_text_container">Quero Receber promoções!</span>
                        </button>
                        <div class="response"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<form id="form-news-letter" class="signup form-inline row mt-3"
      action="<?php echo get_the_permalink(); ?>#form-news-letter" method="post" enctype="multipart/form-data" class="">

</form>