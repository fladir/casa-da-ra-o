<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'casadaracao' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'ciaserver_user' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'ciaserver_mysql' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'CL`Ln|*jMfM~ZYN14HqhRXvBxrRS5Gt*Y1vLq^bo3a8q8#h`P*WT6K3IeQXY u%x' );
define( 'SECURE_AUTH_KEY',  'hdqz8]:|4 yX8rcJeR52Y74&+$j]5@cdXJ)1jWg,!kaI*v0S1aG38FVjBz:H)z{e' );
define( 'LOGGED_IN_KEY',    'EVdi8$ISM?T85Iy^ RQ!QQjZ*88-1k1Cd3e%yrEv dN9F5>6V)FbP=W1/_j d>UQ' );
define( 'NONCE_KEY',        'A?[hn#hiS)~^I(;[eO?<!d6hv4U&u1lyEXJ?FBh[K$)f8U99S^!zD{<KDknlqyW/' );
define( 'AUTH_SALT',        'J,<]Gg9;zX[A8N<+s=MG^cy(.19eoSeQB^obOSg|`!=[*?-e)tDWG%q/f.,b#LPt' );
define( 'SECURE_AUTH_SALT', 'r?|>GQzImVHI/)0@+T>+QG_(c?,V{J|1T8BM6fP`pUsPMgC(*,4+ :c9m@W+RGh~' );
define( 'LOGGED_IN_SALT',   '@nZf$koGYl=`=V)}Z#.PsysR,L6JfQtZafVWEI$SOF~[KIk,lK@ODA]5/$+p7Jy_' );
define( 'NONCE_SALT',       'VN^Jgq|fAh%i3W,^p%I<lQ9|J-j1?#kK(J|nQ(hZMZ.qH|mu^X)SLpZM*5R^Szgm' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'cdr_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';

/** Sets up 'direct' method for wordpress, auto update without ftp */
define('FS_METHOD','direct');